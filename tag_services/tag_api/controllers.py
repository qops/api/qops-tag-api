########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


# Import flask dependencies
from flask import Blueprint, request, render_template, \
                  flash, g, session, redirect, url_for

# Import models
from .models import Tag


# Import the database object from the main app module
from qops_desktop import db

# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_tag = Blueprint('tag', __name__, url_prefix='/tag')

@mod_tag.context_processor
def store():
    store_dict = { 'serviceName': 'Tag',
                   'serviceDashboardUrl': url_for('tag.dashboard'),
                   'serviceBrowseUrl': url_for('tag.browse'),
                   'serviceNewUrl': url_for('tag.new'),
                 }
    return store_dict

# Set the route and accepted methods
@mod_tag.route('/', methods=['GET'])
def tag():
    return render_template('tag/tag_dashboard.html')


@mod_tag.route('/dashboard', methods=['GET'])
def dashboard():
    return render_template('tag/tag_dashboard.html')


@mod_tag.route('/browse', methods=['GET'])
def browse():
    return render_template('tag/tag_browse.html')


@mod_tag.route('/new', methods=['GET', 'POST'])
def new():
    return render_template('tag/tag_new.html')


@mod_tag.route('/profile', methods=['GET', 'POST'])
def profile():
    return render_template('tag/tag_profile.html')


@mod_tag.route('/view', methods=['GET', 'POST'])
def tag_view():
    return render_template('tag/tag_view.html')

